var express = require('express');
var User=require('../models/users');
var router = express.Router();


/* GET users listing. */
//console.log(User());
router.get('/', function(req, res) {
  //console.log(req.db);
  req.db.userData.find({},{},function(e,docs){
      res.render('users/index', {"userlist" : docs});
  });
});

router.get('/new',function(req,res,next){
	res.render('users/new',{title:"New User",error:""})
});

router.post('/',function(req,res,next){
	validate=new validateRegister();
	console.log(validate.email(req));
	if(validate.email(req)){
		next();
	}else{
		res.render('users/new',{message:"Email not valid"});
	}
}, function(req,res){
		req.db.userData.insert({"username":req.body.username, "email":req.body.email});
		res.render('users/new',{message:"Record successfully saved"});
});

var validateRegister=function(){
	this.email = function(req){
		console.log(req);
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  	return re.test(req.body.email);
	}  
}

module.exports = router;
