var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator')

var engine = require('ejs-mate');
var db_connection=require('./services/db_connection');
var middleware=require('./middlewares/index');



var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.engine('ejs', engine);
app.set('views', [path.join(__dirname, 'views'),path.join(__dirname, 'views/users') ]);
app.set('view engine', 'ejs');
//app.set('view options', { layout:'layout.ejs' });
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(middleware(db_connection.db.config()));

app.use('/', index);
app.use('/user', users);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
